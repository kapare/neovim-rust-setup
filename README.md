# neovim rust setup

## rust install

Use curl to install rustup for the first install:

```
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
```

Use rustup to update

```
rust update
```

* https://rustup.rs/

## rust-analyzer install

Install it from the github repos:

```
git clone https://github.com/rust-lang/rust-analyzer.git && cd rust-analyzer
git checkout release
cargo xtask install --server
```

* https://rust-analyzer.github.io/manual.html#rust-analyzer-language-server-binary

## packer install

```
mkdir ~/.local/share/nvim/aur
cd ~/.local/share/nvim/aur 
git clone https://aur.archlinux.org/nvim-packer-git.git 
cd nvim-packer-git
makepkg -si
```

* https://github.com/wbthomason/packer.nvim#quickstart

## neovim plugin

```
mkdir -p ~/.config/nvim/lua 
nvim ~/.config/nvim/lua/plug.lua
```

```
return require('packer').startup(function()
    -- other plugins...
    
    use 'williamboman/mason.nvim'    
    use 'williamboman/mason-lspconfig.nvim'
   
   -- other plugins...
end)
```

```
nvim ~/.config/nvim/init.lua
```

```
require("plug")
require("mason").setup()
```

```
:PackerInstall
:MasonInstall rust-analyzer codelldb
```



* https://rsdlt.github.io/posts/rust-nvim-ide-guide-walkthrough-development-debug/#1-download-rust-analyzer-and-codelldb-with-neovims-plugins
